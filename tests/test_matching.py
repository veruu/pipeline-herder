"""Test matching.py."""
import unittest
from unittest import mock

from pipeline_herder import matching


class TestNotifyFinished(unittest.TestCase):
    """Test notify_finished."""

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.multiple('pipeline_herder.settings',
                         RABBITMQ_PUBLISH_EXCHANGE='exchange',
                         RABBITMQ_PUBLISH_ROUTING_KEY='key')
    def test_call():
        """Check send_message is called correctly."""
        matching.utils.PUBLISH_QUEUE = mock.Mock()

        matching.notify_finished('host', 'cki-project/cki-pipeline', 123)
        matching.utils.PUBLISH_QUEUE.send_message.assert_called_with(
            {
                'gitlab_url': 'host',
                'project': 'cki-project/cki-pipeline',
                'job_id': 123
            },
            'key',
            exchange='exchange'
        )

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_call_no_rabbitmq(self):
        """Check send_message is not called with is_production=False."""
        matching.utils.PUBLISH_QUEUE = mock.Mock()

        matching.notify_finished('host', 'cki-project/cki-pipeline', 123)
        self.assertFalse(matching.utils.PUBLISH_QUEUE.send_message.called)
