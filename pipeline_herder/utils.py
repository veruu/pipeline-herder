"""Various utils used across the pipeline herder."""
import os
import re
from distutils.util import strtobool
from functools import cached_property
from functools import lru_cache

import gitlab
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.session import get_session

from . import settings

LOGGER = get_logger(__name__)
SESSION = get_session(__name__, LOGGER)


class CachedJob:
    """Cache information about a GitLab job to minimize API calls."""

    def __init__(self, gitlab_host, project, job_id):
        """Initialize a cached job."""
        self._gitlab_host = gitlab_host
        self._project = project
        self._job_id = job_id

    def args(self):
        """Return the arguments to reconstruct the instance."""
        return (self._gitlab_host, self._project, self._job_id)

    @cached_property
    def gl_instance(self):
        """Return the GitLab instance."""
        return gitlab.Gitlab(f'https://{self._gitlab_host}',
                             settings.GITLAB_TOKENS[self._gitlab_host],
                             session=SESSION)

    @cached_property
    def gl_project(self):
        """Return the GitLab project."""
        return self.gl_instance.projects.get(self._project)

    @cached_property
    def gl_pipeline(self):
        """Return the GitLab pipeline."""
        return self.gl_project.pipelines.get(self.gl_job.pipeline['id'])

    @cached_property
    def gl_pipeline_jobs(self):
        """Return all jobs of the GitLab pipeline."""
        return self.gl_pipeline.jobs.list(all=True)

    @cached_property
    def variables(self):
        """Return the GitLab pipeline variables."""
        return {v.key: v.value for v in self.gl_pipeline.variables.list()}

    @cached_property
    def retriggered(self):
        """Return whether the pipeline was retriggered."""
        return strtobool(self.variables.get('retrigger', 'false'))

    @cached_property
    def gl_job(self):
        """Return the GitLab job."""
        return self.gl_project.jobs.get(self._job_id)

    @cached_property
    def trace(self):
        """Return the GitLab job trace."""
        return self.gl_job.trace().decode('utf8').split('\n')

    # This does not work yet for artifacts in S3!
    @lru_cache
    def artifact_file(self, name):
        """Return one artifact file or an empty list if not found."""
        try:
            return self.gl_job.artifact(name).decode('utf8').split('\n')
        # pylint: disable=broad-except
        except Exception:
            return []

    @cached_property
    def auth_user_id(self):
        """Return the user owning the GitLab connection."""
        instance = self.gl_instance
        if not hasattr(instance, 'user'):
            instance.auth()
        return instance.user.id

    def job_name_count(self):
        """Return the number of jobs in the pipeline with the same name."""
        return len([j for j in self.gl_pipeline_jobs
                    if j.name == self.gl_job.name])

    def retry_delay(self):
        """Return the number of minutes to wait before a retry."""
        delay_index = self.job_name_count() - 1
        if delay_index >= len(settings.HERDER_RETRY_DELAYS):
            return settings.HERDER_RETRY_DELAYS[-1]
        return settings.HERDER_RETRY_DELAYS[delay_index]

    def is_retry_unsafe(self):
        # pylint: disable=too-many-return-statements
        """Check whether a job can be safely retried by the herder.

        Returns None if the job can be retried or a string with the error
        message.
        """
        # do not retry retriggered pipelines
        if self.retriggered:
            return 'retriggered job'

        # each job must have been started by pipeline owner or herder bot
        gl_pipeline = self.gl_pipeline
        allowed_users = (gl_pipeline.user['id'], self.auth_user_id)
        external_users = set(j.user['username'] for j in self.gl_pipeline_jobs
                             if j.user['id'] not in allowed_users)
        if external_users:
            return f'external users interfered: {", ".join(external_users)}'

        # there should be no newer job with the same name
        newer_jobs = [f'J{j.id}' for j in self.gl_pipeline_jobs
                      if j.name == self.gl_job.name and j.id > self.gl_job.id]
        if newer_jobs:
            return f'job restarted externally as {", ".join(newer_jobs)}'

        # only retry up to a maximum number of times
        if self.job_name_count() > settings.HERDER_RETRY_LIMIT:
            return f'maximum of {settings.HERDER_RETRY_LIMIT} retries reached'

        # only retry if the herder is configured to do so
        if settings.HERDER_ACTION != 'retry':
            return f'HERDER_ACTION={settings.HERDER_ACTION}'

        if not misc.is_production():
            return 'IS_PRODUCTION=False'

        return None


class Matcher:  # pylint: disable=too-few-public-methods
    """Base class for matchers."""

    # pylint: disable=too-many-arguments
    def __init__(self, name, description, messages,
                 job_name=None, file_name=None,
                 action='retry', tail_lines=100):
        """Initialize the matcher."""
        self.name = name
        self.description = description
        self.job_name = job_name
        self.file_name = file_name
        self.action = action
        self.messages = messages if isinstance(messages, list) else [messages]
        self.tail_lines = tail_lines

    @staticmethod
    def _check_lines(message, lines):
        """Check lines for a match."""
        if isinstance(message, re.Pattern):
            return message.search('\n'.join(lines))
        return any(message in line for line in lines)

    def check_lines(self, lines):
        """Check lines for a match."""
        return any(self._check_lines(m, lines) for m in self.messages)

    def check(self, job: CachedJob):
        """Check status/log for a match."""
        if job.gl_job.status != 'failed':
            return False
        if self.job_name and not job.gl_job.name.startswith(self.job_name):
            return False
        if self.file_name:
            all_lines = job.artifact_file(self.file_name)
        else:
            all_lines = job.trace

        return self.check_lines(all_lines[-self.tail_lines:])


def notify_irc(job: CachedJob, notification):
    """Send a message to the IRC bot."""
    with misc.only_log_exceptions():
        status = job.gl_job.status
        name = job.gl_job.name
        job_id = job.gl_job.id
        pipeline_id = job.gl_pipeline.id
        retriggered = ' (retriggered)' if job.retriggered else ''
        url = misc.shorten_url(job.gl_job.web_url)
        message = (f'🤠 P{pipeline_id} J{job_id}{retriggered} {name} {status}:'
                   f' {notification} {url}')
        LOGGER.info('%s', message)
        if settings.IRCBOT_URL:
            SESSION.post(settings.IRCBOT_URL, json={'message': message})


RABBITMQ_PUBLISH_HOST = os.environ.get('RABBITMQ_PUBLISH_HOST')
RABBITMQ_PUBLISH_PORT = misc.get_env_int('RABBITMQ_PUBLISH_PORT', 0)
RABBITMQ_PUBLISH_USER = os.environ.get('RABBITMQ_PUBLISH_USER')
RABBITMQ_PUBLISH_PASSWORD = os.environ.get('RABBITMQ_PUBLISH_PASSWORD')

PUBLISH_QUEUE = MessageQueue(RABBITMQ_PUBLISH_HOST, RABBITMQ_PUBLISH_PORT,
                             RABBITMQ_PUBLISH_USER, RABBITMQ_PUBLISH_PASSWORD)
