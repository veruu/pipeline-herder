"""Pipeline herder matching tasks."""
import argparse
import os
import re
import sys
from urllib import parse

import sentry_sdk
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue

from . import herding
from . import matchers
from . import settings
from . import utils

LOGGER = get_logger('cki.pipeline_herder.matching')


def process_webhook(_, message):
    """Process a job from a GitLab webhook."""
    object_kind = message['object_kind']
    if object_kind != 'build':
        LOGGER.info('Ignoring %s for %s', object_kind, message["project_name"])
        return
    status = message['build_status']
    if status not in ('success', 'failed'):
        LOGGER.info('Ignoring %s for %s', status, message["project_name"])
        return
    homepage = parse.urlparse(message['repository']['homepage'])
    process_job(homepage[1], homepage[2].lstrip('/'), message['build_id'])


def process_job(gitlab_host, project, job_id):
    """Process a job directly specified via host/project/id."""
    job = utils.CachedJob(gitlab_host, project, job_id)
    job_id = job.gl_job.id
    LOGGER.info('Processing P%s J%s', job.gl_pipeline.id, job_id)
    for matcher_instance in matchers.MATCHERS:
        description = matcher_instance.description
        if not matcher_instance.check(job):
            continue
        if matcher_instance.action == 'report':
            utils.notify_irc(job, f'Detected {description}')
            return 'report'
        if matcher_instance.action == 'retry':
            unsafe_reason = job.is_retry_unsafe()
            if unsafe_reason:
                utils.notify_irc(job, f'Detected {description}, '
                                 f'not retrying: {unsafe_reason}')
                return 'report'
            delay = job.retry_delay()
            if delay == 0:
                utils.notify_irc(job, f'Detected {description}, retrying now')
            else:
                utils.notify_irc(job, f'Detected {description}, '
                                 f'retrying in {delay} minutes')
            submit_retry(job, matcher_instance, delay)
            return 'retry'

        utils.notify_irc(
            job, (f'Detected {description}. '
                  f'🔥 Action \'{matcher_instance.action}\' not handled 🔥')
        )
        return 'error'

    notify_finished(gitlab_host, project, job_id)
    return None


def notify_finished(gitlab_host, project, job_id):
    """Add finished job to the message queue."""
    if not misc.is_production():
        LOGGER.info('Not notifying via amqp because of non-production env')
        return
    utils.PUBLISH_QUEUE.send_message(
        {
            'gitlab_url': gitlab_host,
            'project': project,
            'job_id': job_id,
        },
        settings.RABBITMQ_PUBLISH_ROUTING_KEY,
        exchange=settings.RABBITMQ_PUBLISH_EXCHANGE
    )


def submit_retry(job, _, delay):
    """Submit the suggested actions with the specified delay in minutes."""
    herding.retry.apply_async(job.args(), countdown=delay * 60)


def process_queue():
    """Process jobs from the message queue."""
    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    MessageQueue().consume_messages(
        settings.RABBITMQ_WEBHOOKS_EXCHANGE,
        settings.RABBITMQ_WEBHOOKS_ROUTING_KEYS,
        process_webhook,
        queue_name=settings.RABBITMQ_WEBHOOKS_QUEUE)


def process_single(job_url):
    """Process a single job from the command line."""
    url_parts = parse.urlparse(job_url)
    gitlab_host = url_parts.hostname
    project = re.sub('/-/.*', '', url_parts.path[1:])
    job_id = re.sub('.*/', '', url_parts.path)
    job = utils.CachedJob(gitlab_host, project, job_id)
    for matcher_instance in matchers.MATCHERS:
        if not matcher_instance.check(job):
            continue
        print(matcher_instance.description)
        return
    print('Nothing matched 😕')


def main(argv):
    """CLI Interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--job-url',
                        help='Try the matcher for an individual job.')
    args = parser.parse_args(argv)

    if args.job_url:
        process_single(args.job_url)
    else:
        process_queue()


if __name__ == '__main__':
    main(sys.argv[1:])
