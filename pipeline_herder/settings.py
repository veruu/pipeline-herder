"""Settings read from env variables."""
import json
import os

from cki_lib import misc

HERDER_ACTION = os.environ.get('HERDER_ACTION', 'report')
HERDER_RETRY_LIMIT = misc.get_env_int('HERDER_RETRY_LIMIT', 3)
HERDER_RETRY_DELAYS = [
    int(d) for d in
    os.environ.get('HERDER_RETRY_DELAYS', '0,3,10').split(',')]

SHORTENER_URL = os.environ.get('SHORTENER_URL')
IRCBOT_URL = os.environ.get('IRCBOT_URL')

GITLAB_TOKENS = {
    h: os.environ.get(t)
    for h, t in json.loads(os.environ.get('GITLAB_INSTANCES', '{}')).items()
}

RABBITMQ_PUBLISH_EXCHANGE = os.environ.get('RABBITMQ_PUBLISH_EXCHANGE')
RABBITMQ_PUBLISH_ROUTING_KEY = os.environ.get('RABBITMQ_PUBLISH_ROUTING_KEY')

RABBITMQ_WEBHOOKS_EXCHANGE = os.environ.get(
    'WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks')
RABBITMQ_WEBHOOKS_ROUTING_KEYS = os.environ.get(
    'PIPELINE_HERDER_WEBHOOKS_ROUTING_KEYS', '').split()
RABBITMQ_WEBHOOKS_QUEUE = os.environ.get(
    'PIPELINE_HERDER_WEBHOOKS_QUEUE')
