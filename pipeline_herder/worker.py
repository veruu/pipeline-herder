"""Pipeline herder worker."""
import os

import sentry_sdk
from celery import Celery
from sentry_sdk.integrations.celery import CeleryIntegration

if os.environ.get('FLASK_ENV', 'production') == 'production':
    # the DSN is read from SENTRY_DSN
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[CeleryIntegration()]
    )

app = Celery('pipeline-herder', broker='redis://localhost')
app.autodiscover_tasks(['pipeline_herder.herding', 'pipeline_herder.matching'],
                       related_name=None)
